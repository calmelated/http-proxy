var http = require('http');
var httpProxy = require('http-proxy');
var fs = require('fs');
var conf = JSON.parse(fs.readFileSync('http-proxy.conf', 'utf-8'));

var proxy = httpProxy.createProxyServer({});
var server = http.createServer(function(req, res) {
    proxy.web(req, res, { target: conf.forward });
});

if(conf.debug) {
    proxy.on('start', function(proxyReq, req, res, options) {
        proxyReq.on('data' , function(dataBuffer){
            var d = new Date(); d = d.getFullYear() + '/' + d.getMonth() + '/'+ (d.getDay() + 1) + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            console.log(d + ' -> ' + decodeURIComponent(dataBuffer.toString('utf8')));
        });
    });

    proxy.on('proxyRes', function (proxyRes, req, res) {
        proxyRes.on('data' , function(dataBuffer){
            var d = new Date(); d = d.getFullYear() + '/' + d.getMonth() + '/'+ (d.getDay() + 1) + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            console.log(d + ' <- ' + decodeURIComponent(dataBuffer.toString('utf8')));
        });
    });
}

proxy.on('error', function (e) {
    console.log(e.stack);
});

console.log("Listening on port " + conf.listen_port);
server.listen(conf.listen_port);
